﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Switch(ref int x, ref int y)
        {
            var t = x;
            x = y;
            y = t;
        }

        static int Partition(int[] array, int minIndex, int maxIndex)
        {
            var pivo = minIndex - 1;
            for (var i = minIndex; i < maxIndex; i++)
            {
                if (array[i] < array[maxIndex])
                {
                    pivo++;
                    Switch(ref array[pivo], ref array[i]);
                }
            }

            pivo++;
            Switch(ref array[pivo], ref array[maxIndex]);
            return pivo;
        }

        static int[] Quick(int[] array, int minIndex, int maxIndex)
        {
            if (minIndex >= maxIndex)
            {
                return array;
            }

            var pivoIndex = Partition(array, minIndex, maxIndex);
            Quick(array, minIndex, pivoIndex - 1);
            Quick(array, pivoIndex + 1, maxIndex);

            return array;
        }

        static int[] Quick(int[] array)
        {
            return Quick(array, 0, array.Length - 1);
        }

        static void Main(string[] args)
        {
            Console.Write("N = ");
            var len = Convert.ToInt32(Console.ReadLine());
            var a = new int[len];
            for (var i = 0; i < a.Length; ++i)
            {
                Console.Write("a[{0}] = ", i);
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Впорядкований масив: {0}", string.Join("; ", Quick(a)));

            Console.ReadLine();
        }
    }
}
